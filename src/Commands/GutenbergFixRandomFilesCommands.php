<?php

namespace Drupal\gutenberg_fix_randomfiles\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Database\Connection;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drush\Commands\DrushCommands;

/**
 * The GutenbergFixRandomFiles Drush commandfile.
 */
class GutenbergFixRandomFilesCommands extends DrushCommands {

  /**
   * The state key containing the files which were updated.
   */
  public const STATE_KEY = 'gutenberg_update.random_files';

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Attempt to move random files to their appropriate names.
   *
   * As well as updating references to them in other entities e.g. text content.
   *
   * @command gutenberg_fix_randomfiles:update
   */
  public function update() {
    // Switch to user 1 in order to bypass the FileUpdateService::initService
    // permission check.
    $account = User::load(1);
    \Drupal::service('account_switcher')->switchTo($account);

    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $database = \Drupal::database();
    $query = $database->select('file_managed', 'fm')
      ->fields('fm', ['fid', 'filename', 'uri'])
      // The random Gutenberg files are 50 characters long.
      // https://git.drupalcode.org/project/gutenberg/-/blob/042d0569bf39f5440c4754785c50a097814a2e0b/src/MediaUploader.php#L72
      // The uri filename is 50 alphanumeric characters long.
      ->condition('fm.uri', '\/[A-Za-z0-9]{50}\.[A-Za-z0-9]+$', 'REGEXP');
    $uri_column = $database->escapeField('fm.uri');
    $filename_column = $database->escapeField('fm.filename');
    // And the filename doesn't match the current one.
    $query->where("SUBSTRING_INDEX(SUBSTRING_INDEX($uri_column, :delimiter1, :limit1), :delimiter2, :limit2) != $filename_column", [
      ':delimiter1' => '/',
      ':limit1' => -1,
      ':delimiter2' => '/',
      ':limit2' => -1,
    ]);
    $query->orderBy('fm.filename');
    $files = $query->execute();

    $migrated_files = \Drupal::state()->get(static::STATE_KEY, []);
    $updated_files = [];
    /** @var \Drupal\file_update\FileUpdateService $update_service */
    $update_service = \Drupal::service('file_update.update_service');
    foreach ($files as $file) {
      $fid = (int) $file->fid;
      $src = $file->uri;
      $filename = $file->filename;
      if (strlen(pathinfo($file_system->basename($src), PATHINFO_FILENAME)) !== 50) {
        // Ensure that this matches the actual basename even though the regex
        // realistically should already have.
        continue;
      }
      $destination = $file_system->dirname($src) . '/' . $filename;
      if ($src !== $destination) {
        // Attempt to move to the new destination.
        if (!$update_service->prepareNewUri($fid, $destination)) {
          $this->logger()->warning('File %fid (%uri) could not be moved to %destination.', [
            '%fid' => $fid,
            '%uri' => $src,
            '%filename' => $filename,
            '%destination' => $destination,
          ]);
          continue;
        }

        if ($update_service->updateFile($fid, ['move_to' => $destination])) {
          $file_entity = $update_service->getCurrentFileEntity();
          if (!$file_entity) {
            $this->logger()->warning(dt('Could not load file %id (%src).', [
              '%id' => $fid,
              '%uri' => $src,
            ]));
            continue;
          }
          $real_destination = $file_entity->getFileUri();
          if ($real_destination === $src) {
            $this->logger()->warning(dt('The destination file (%id): %uri has not changed.', [
              '%id' => $fid,
              '%uri' => $src,
            ]));
            continue;
          }

          $node_ids = $this->updateAffectedNodes($src, $database, $real_destination, $fid);

          $updated_files[$fid] = [
            'fid' => $fid,
            'src' => $src,
            'dest' => $real_destination,
            'nids' => $node_ids,
          ];
          // Persist the current status as soon as possible.
          \Drupal::state()->set(static::STATE_KEY, $updated_files + $migrated_files);

          $this->logger()->info(dt('Migrated %src => %dest', [
            '%src' => $src,
            '%dest' => $real_destination,
          ]));
        }
        else {
          $this->logger()->warning(
            dt('Could not update file %id (%uri).', [
              '%id' => $fid,
              '%uri' => $src,
            ])
          );
        }
      }
    }

    $this->logger()->notice(
      dt('Updated @count file(s).', ['@count' => count($updated_files)])
    );
  }

  /**
   * Attempt to re-update nodes with references to the updated files.
   *
   * Useful during development, or after manually massaging the data state
   * data.
   *
   * @command gutenberg_fix_randomfiles:reupdate-nodes
   */
  public function updateExistingNodes() {
    $files = \Drupal::state()->get(static::STATE_KEY, []);
    $updated = 0;
    $database = \Drupal::database();
    foreach ($files as &$file) {
      $fid = $file['fid'];
      $src = $file['src'];
      $dest = $file['dest'];
      $affected_nodes = $this->updateAffectedNodes($src, $database, $dest, $fid);
      $updated += count($affected_nodes);
      if ($affected_nodes) {
        $file['nids'] = array_unique(array_merge($file['nids'] ?? [], $affected_nodes));
        // Persist the current status as soon as possible.
        \Drupal::state()->set(static::STATE_KEY, $files);
      }
    }
    unset($file);

    $this->logger()->notice(
      dt('Updated @count node(s).', ['@count' => $updated])
    );
  }

  /**
   * Provides a list of migrated files and their destinations.
   *
   * Which might be useful for generating redirects if necessary.
   * NB: It doesn't include the potentially generated image styles.
   *
   * @command gutenberg_fix_randomfiles:list
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   The list of affected files.
   *
   * @format table
   * @pipe-format csv
   * @default-fields fid,src_path,dest_path
   * @field-labels
   *   fid: File ID
   *   src: Source URI
   *   dest: Destination URI
   *   src_path: Source path
   *   dest_path: Destination path
   *   nids: Node IDs.
   */
  public function list() {
    $affected = \Drupal::state()->get(static::STATE_KEY, []);
    $result = array_map(function ($entry) {
      return $entry + [
        'src_path' => $this->fileUrlGenerator()->generateString($entry['src']),
        'dest_path' => $this->fileUrlGenerator()->generateString($entry['dest']),
      ];
    }, $affected);

    return new RowsOfFields($result);
  }

  /**
   * Provides a list of migrated files and their usage references.
   *
   * @command gutenberg_fix_randomfiles:usage
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   The list of affected files.
   *
   * @format table
   * @pipe-format csv
   * @default-fields fid,uri,usage
   * @field-labels
   *   fid: File ID
   *   uuid: UUID
   *   uri: URI
   *   usage: Usage
   */
  public function listUsageReference() {
    $affected = \Drupal::state()->get(static::STATE_KEY, []);
    $result = [];
    /** @var \Drupal\file\FileUsage\FileUsageInterface $file_usage */
    $file_usage = \Drupal::service('file.usage');
    $entity_type_manager = \Drupal::entityTypeManager();
    foreach ($affected as $item) {
      if ($file = File::load($item['fid'])) {
        $fid = $file->id();
        $result[$fid]['fid'] = $fid;
        $result[$fid]['uuid'] = $file->uuid();
        $result[$fid]['uri'] = $file->getFileUri();
        $result[$fid]['usage'] = [];
        if (!empty($item['nids'])) {
          foreach ($item['nids'] as $nid) {
            $result[$fid]['usage'][] = "node/$nid";
          }
        }
        /* @see file_get_file_references() */
        $usage_list = $file_usage->listUsage($file);
        foreach ($usage_list as $usage_list_item) {
          foreach ($usage_list_item as $entity_type => $references) {
            $entities = $entity_type_manager->getStorage($entity_type)->loadMultiple(array_keys($references));
            foreach ($entities as $entity) {
              if ($entity->hasLinkTemplate('canonical')) {
                $result[$fid]['usage'][] = $entity->toUrl()->getInternalPath();
              }
              else {
                $entity_id = $entity->id();
                $result[$fid]['usage'][] = "${entity_type}:${entity_id}";
              }
            }
          }
        }
        $result[$fid]['usage'] = implode(', ', $result[$fid]['usage']);
      }
    }

    return new RowsOfFields($result);
  }

  /**
   * Update affected nodes with hardcoded references to the original URLs.
   *
   * @param string $src
   *   The source URI.
   * @param \Drupal\Core\Database\Connection $database
   *   The Drupal database.
   * @param string $destination
   *   The real destination URI.
   * @param int $fid
   *   The file ID.
   *
   * @return array
   *   The affected node IDs.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function updateAffectedNodes($src, Connection $database, $destination, $fid) {
    $source_url = $this->fileUrlGenerator()->generateString($src);
    // Attempt to pick up files which were created without reference to
    // the uuid. e.g. core/file.
    // https://www.drupal.org/project/gutenberg/issues/3106598
    $like = '%' . $database->escapeLike($source_url) . '%';
    $node_ids = \Drupal::entityQuery('node', 'OR')
      ->accessCheck(FALSE)
      ->condition('body', $like, 'LIKE')
      ->execute();
    if ($node_ids) {
      /** @var \Drupal\node\NodeInterface[] $nodes */
      $nodes = Node::loadMultiple($node_ids);
      $dest = $this->fileUrlGenerator()->generateString($destination);
      foreach ($nodes as $node) {
        $this->logger()->info(
          dt(
            'Update Node %nid body and summary content references to file (%id): %uri',
            [
              '%id' => $fid,
              '%nid' => $node->id(),
              '%uri' => $src,
            ]
          )
        );
        // Replace references to the old URI.
        $body_field = $node->get('body');
        $values = $body_field->getValue();
        foreach ($values as $key => $value) {
          $properties = ['value'];
          if ($body_field->getFieldDefinition()->getType() === 'text_with_summary') {
            // Include the summary field.
            $properties[] = 'summary';
          }
          foreach ($properties as $property) {
            if (!empty($value[$property])) {
              $original = $value[$property];
              // Would ideally do this with the Gutenberg parser, but there's no
              // easy way to manipulate the comments effectively.
              $values[$key][$property] = str_replace($source_url, $dest, $original);
            }
          }
        }
        $body_field->setValue($values);
        $node->setRevisionLogMessage(
          "gutenberg_fix_randomfiles update ($source_url) reference"
        );
        $node->save();
      }
    }

    return $node_ids;
  }

  /**
   * Returns the file URL generator.
   *
   * @return \Drupal\Core\File\FileUrlGeneratorInterface
   *   The file URL generator.
   */
  protected function fileUrlGenerator() {
    if ($this->fileUrlGenerator === NULL) {
      $this->fileUrlGenerator = \Drupal::service('file_url_generator');
    }

    return $this->fileUrlGenerator;
  }

}
