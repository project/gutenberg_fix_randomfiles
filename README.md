CONTENTS OF THIS FILE
---------------------

* Introduction
  * Disclaimer
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The "Gutenberg Random Filename Fix" module provides a way to migrate content
affected by the [3208407](https://www.drupal.org/project/gutenberg/issues/3208407)
Gutenberg bug.


### DISCLAIMER

A database AND file system backup is **HIGHLY** recommended as the module makes
changes directly to the existing content and file system - there are no undos.

So local tests should be done before running this on a production server.


REQUIREMENTS
------------

This module requires [Drush](https://www.drush.org/latest/) and the
[File update](https://www.drupal.org/project/file_update) module to function.

As well as the relevant patches from these issues:

- https://www.drupal.org/project/file_update/issues/3214405
- https://www.drupal.org/project/file_update/issues/3214409

You may do so by adding the patches to your `composer.json` file:

```yaml
{
  // ...
  "extra": {
    "patches": {
      "drupal/file_update": {
        "#3214405 - Make use of the appropriate destination file": "https://www.drupal.org/files/issues/2021-05-18/file_update-use-appropriate-destination-file-3214405-4.diff",
        "#3214409 - Improve the Editor plugin": "https://www.drupal.org/files/issues/2021-05-18/file_update-improve-editor-plugin-3214409-4.diff"
      }
    }
  }
}
```

And applying it with the [composer-patches](https://github.com/cweagans/composer-patches) plugin.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

Add the following to your local `settings.php` file.

```php
// Provide support for updating Gutenberg's Gallery block type.
$settings['file_update.editor_attributes'] = ['data-full-url', 'data-link'];
```

USAGE
-----

```shell
# Attempt to move random files to their locations with appropriate names.
drush gutenberg_fix_randomfiles:update

# Provides a list of migrated files and their destinations.
drush gutenberg_fix_randomfiles:list

# Provides a list of migrated files and their usage references.
drush gutenberg_fix_randomfiles:usage
```

LOCAL DEVELOPMENT
-----------------

Local development/testing process (using [Lando](https://github.com/lando/lando)):
```shell
# Import a clean database.
lando db-import database.sql.gz
# Delete the existing files
rm -rf web/sites/default/files/*
# Copy/extract the files assets
tar -xvf files-uploads.tar -C web/sites/default/files/
# Apply the update.
lando drush gutenberg_fix_randomfiles:update

# List the affected files and their usage.
drush gutenberg_fix_randomfiles:usage
```

This process assumes you already have a dump of your database and files
directory.

CONFIGURATION
-------------

N/A


MAINTAINERS
-----------

* codebymikey - https://www.drupal.org/u/codebymikey

Supporting organization:

* Zodiac Media - https://www.drupal.org/zodiac-media
